import React, { Component } from 'react';
import './App.css';
import Quote from './components/Quote';
import Subnav from './components/Subnav';


class App extends Component {
  render() {
    return (
      <div className="App">
          <Quote/>
          <Subnav/>
      </div>
    );
  }
}

export default App;
