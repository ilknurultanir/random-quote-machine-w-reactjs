import React from 'react' ;
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink} from 'reactstrap';

class Subnav extends React.Component{
    constructor(props) {
        super(props);
    
        this.toggle = this.toggle.bind(this);
        this.state = {
          isOpen: false
        };
      }
      toggle() {
        this.setState({
          isOpen: !this.state.isOpen
        });
      }
    render(){
        return(
            <div>
        <Navbar light expand="md" className="fixed-bottom">
          <NavbarBrand href="/">Random Quote Machine <br/> w React.js </NavbarBrand>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <NavLink href="https://bitbucket.org/ilknurultanir/random-quote-machine-w-reactjs">Check this project on Bitbucket </NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="https://github.com/iibarbari">My Github</NavLink>
              </NavItem>
            </Nav>
          </Collapse>
        </Navbar>
      </div>
        );
    }
}

export default Subnav;