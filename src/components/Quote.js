import React from 'react';
import {
    Button,
    Container,
    Row,
    Col,
    ButtonGroup
} from 'reactstrap';

var bgClasses=["rose-water","monte-carlo","lemon-twist","green-beach","sunny-days","harmonic-energy"];

class QuoteMachine extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            quote: '',
            author:'',
            bgColor:''
        };
        this.handleClick = this.handleClick.bind(this);

    }
    componentDidMount() {
        var body = document.getElementsByTagName("body")[0];
        document.body.className = '';
        body.classList.add(bgClasses[Math.floor(Math.random() * bgClasses.length)]);

        fetch("https://talaikis.com/api/quotes/random/ ")
          .then(res => res.json())
          .then(
            (result) => {
              this.setState({
                quote: result.quote,
                author: result.author,
              });
            },
            // Note: it's important to handle errors here
            // instead of a catch() block so that we don't swallow
            // exceptions from actual bugs in components.
            (error) => {
              this.setState({
               error
              });
            }
          )
      }
    
    handleClick() {
        var body = document.getElementsByTagName("body")[0];
        document.body.className = '';
        body.classList.add(bgClasses[Math.floor(Math.random() * bgClasses.length)]);


        fetch("https://talaikis.com/api/quotes/random/ ")
          .then(res => res.json())
          .then(
            (result) => {
              this.setState({
                quote: result.quote,
                author: result.author,
              });
            },
            // Note: it's important to handle errors here
            // instead of a catch() block so that we don't swallow
            // exceptions from actual bugs in components.
            (error) => {
              this.setState({
                isLoaded: true,
                error
              });
            }
          )
    }

    render() {
return ( 
        <div>
            <Container id="quote-box" className="p-5 text-white">
                <Row className="pb-5">
                    <Col>
                        <h1 id="text"> {this.state.quote} </h1> 
                        <h6 className="text-center" id="author"> {this.state.author} </h6> 
                    </Col>
                </Row>
                <Row  >
                    <Col className="text-center">
                        <ButtonGroup size="lg">
                            <Button onClick={ this.handleClick } id="new-quote">New Quote</Button>
                            <Button ><a id="tweet-quote" className="text-white" href={"https://twitter.com/intent/tweet?hashtags=quoteoftheday&text="+ this.state.quote + " "+ this.state.author} target="_blank">Twitter</a></Button>
                        </ButtonGroup>
                    </Col>
                </Row>
            </Container>
        </div>
        )
    }
}

export default QuoteMachine;